module.exports = class Filter {

    startsWith = [];

    equals = [];

    contains = [];

    bot = {};

    constructor(bot) {
        if (this.constructor === Filter) {
            throw new TypeError('Abstract class "Filter" cannot be instantiated directly');
        }

        // A reference to the original Bot (the parent which instanciate the filters)
        // is usefull to get some extra informations. For example, client, channel,
        // members...
        this.bot = bot;
    }

    filter(msg) {
        const params = msg.content.split(' ');

        // Trigger action if message is equals with one of
        // the given words in this.equals
        for(let i = 0; i < this.equals.length; i++) {
            if (this.equals[i] == msg.content) {
                return this.action(msg);
            }
        }

        // Trigger action if message starts with one of
        // the given words in this.startsWith

        if (this.startsWith.includes(params[0])) {
            return this.action(msg);
        }


        // Trigger action if message contains one of
        // the given words in this.contains

        if (this.contains.some(c => msg.content.indexOf(c) === -1)) {
            return this.action(msg);
        }

        // A filter with NO rule ?
        // ok, let's assume it's mandatory (why not ?)

        if (this.startsWith.length == 0 && this.contains.length == 0) {
            return this.action(msg);
        }

        return false;
    }


    action(msg) {
        console.log(msg);
        return true;
    }

};
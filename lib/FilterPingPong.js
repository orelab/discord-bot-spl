const Filter = require('./Filter');

module.exports = class PingPong extends Filter {

    startsWith = ['ping'];

    action(msg) {
        msg.reply('pong');

        return true;
    }

};
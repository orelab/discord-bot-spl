const Filter = require('./Filter');

module.exports = class Question extends Filter {

    equals = ['quoi', 'quoi?', 'quoi ?', 'hein', 'hein?', 'hein ?', 'comment', 'comment?', 'comment ?'];

    action(msg) {
        let response = '';
        switch (msg.content) {
            case 'quoi':
            case 'quoi?':
            case 'quoi ?':
                response = 'feur';
                break;
            case 'hein':
            case 'hein?':
            case 'hein ?':
                response = 'stragram';
                break;
            case 'comment':
            case 'comment?':
            case 'comment ?':
                response = 'taire';
                break;
        }

        if (response) {
            msg.reply(response);
        }

        return !!response;
    }

};
const Filter = require('./Filter');

module.exports = class Google extends Filter {

    contains = ['est-ce', 'pourquoi', 'comment', 'besoin', 'aide'];

    action(msg) {
        const question = msg.content.trim();
        // msg.reply('https://www.google.com/search?q=' + encodeURI(question));
        msg.reply('https://duckduckgo.com/?q=' + encodeURI(question));

        return true;
    }

};
const Filter = require('./Filter');
const Perspective = require('perspective-api-client');

const perspective = new Perspective({ apiKey: process.env.PERSPECTIVE_API_KEY });


module.exports = class Moderation extends Filter {

    startsWith = [];

    contains = [];

    action(msg) {
        return perspective.analyze(msg.content)
            .then(result => {
                if (result
                    && result.attributeScores
                    && result.attributeScores.TOXICITY
                    && result.attributeScores.TOXICITY.summaryScore
                    && result.attributeScores.TOXICITY.summaryScore.value) {

                    const score = result.attributeScores.TOXICITY.summaryScore.value;

                    console.log('Score perspective : ' + score);

                    if (score > process.env.PERSPECTIVE_RATIO) {
                        msg.reply(this.randomMessage());
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    console.log(JSON.stringify(result, null, 2));
                }
                return true;
            })
            .catch(() => {
                console.log(`Smith don't understand "${msg.content}" :/`);
            });
    }

    randomMessage() {
        let message = '';

        switch (Math.round(Math.random() * 5)) {
            case 0: message = 'Quoi ???'; break;
            case 1: message = 'Merci de tenir un language approprié ici...'; break;
            case 2: message = 'Merci de relire la charte d\'utilisation de l\'apprenant à Simplon.'; break;
            case 3: message = 'Un langage correct, non grossier, et bienveillant est apprécié ici.'; break;
            default: message = 'Langage correct exigé.'; break;
        }
        return message;
    }

};
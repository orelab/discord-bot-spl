const Filter = require('./Filter');

module.exports = class What extends Filter {

    // No rule : it's mandatory !
    // (see Filter.js)

    action(msg) {
        let response = '';

        switch (Math.round(Math.random() * 4)) {
            case 1: response = 'Pas compris...'; break;
            case 2: response = 'Quoi ?'; break;
            case 3: response = 'Hein ?'; break;
            default: response = 'Demande-moi de l\'aide avec \'?\''; break;
        }
        msg.reply(response);

        return true;
    }

};
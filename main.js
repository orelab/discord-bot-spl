require('dotenv').config();
const Bot = require('./lib/Bot');
const CronJob = require('cron').CronJob;
const axios = require('axios');
const http = require('http');

// -- BOT

const Smith = new Bot(process.env.DISCORD_TOKEN);

// -- CRON

const bankHoliday = async () => {

    // https://calendrier.api.gouv.fr/jours-feries/
    const year = (new Date()).getFullYear();
    const api = `https://calendrier.api.gouv.fr/jours-feries/metropole/${year}.json`;
    const days = await axios(api);

    const today = (new Date()).toISOString().split('T')[0];

    if (!('data' in days)) return;

    if (today in days.data) {
        Smith.say(days.data[today] + ' !');
    }
};

const messages = [
    // { cron: '0  0  9 * * 1', message: 'Debout là-dedans, c\'est parti pour une nouvelle semaine de folie !' },
    // { cron: '0 55  8 * * 2-5', message: 'Bonjour ! Au boulot !' },
    // { cron: '0 32 12 * * 1-5', message: 'Bon appétit !' },
    // { cron: '0 25 13 * * 1-5', message: 'Finissez votre café, au boulot !' },
    // { cron: '0  5 17 * * 1-4', message: 'Dehors tout le monde, à demain !' },
    { cron: '0 30 17 * * 5', message: 'Bon week-end !' },
    { cron: '0 10 10 * * *', action: bankHoliday },
];

messages.map(m => {

    const job = new CronJob(m.cron, () => {
        if ('message' in m) {
            Smith.say(m.message);
        }

        if ('action' in m) {
            m.action();
        }
    }, null, true, 'Europe/Paris');

    job.start();
});


// Keep app opened as a webserver (render.com web dyno)
// This server is called every 10 minutes by an external
// server with cron to ensure the dyno remains up

http.createServer((req, res)=>{
    res.end('hi!');
}).listen(process.env.WEBDYNO_PORT, '0.0.0.0');

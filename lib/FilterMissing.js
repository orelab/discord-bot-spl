const Filter = require('./Filter');

module.exports = class Missing extends Filter {

    startsWith = ['missing', 'absent'];

    action(msg) {
        const members = this.bot.members()
            .filter(u => u.user.presence.status != 'online')
            .map(u => u.user.username);

        msg.reply(members.length ? members : 'Personne ne manque !');

        return true;
    }

};
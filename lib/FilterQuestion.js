const Filter = require('./Filter');

module.exports = class Question extends Filter {

    startsWith = ['est-ce'];

    action(msg) {
        let response = '';

        switch (Math.round(Math.random() * 6)) {
            case 1: response = 'Oui'; break;
            case 2: response = 'Non'; break;
            case 3: response = 'Peut-être bien'; break;
            case 4: response = 'Oui'; break;
            case 5: response = 'Non'; break;
            case 6: response = 'ça m\'étonnerai'; break;
            default: response = 'Demande-moi de l\'aide avec \'?\''; break;
        }
        msg.reply(response);

        return true;
    }

};
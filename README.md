
# A Discord bot to get life better (or not)


## Installation

First, create a Bot in Discord (see documentation below) and get your > TOKEN <

Then, install and run the app :
```
git clone git@gitlab.com:orelab/discord-bot-spl.git
cd discord-bot-spl
npm install
cp .env.sample .env
# configure .env file
npm run dev
```

## Notice

You can talk to the bot to give him orders :
- '?' will draw a help message
- 'ping', the bot respond 'pong'
- 'bonjour', the bot is polite...
- 'random' followed by a number will show groups of x persons
- 'missing' will show the list of missing or inactive people
- ...

The bot will manage groups with all the people who are part 
of the current group, and who doesn't own ADMINISTRATOR role.

The Perspective API is used to moderate toxic messages.
You can configure the tolerance of the filter with the key
PERSPECTIVE_RATIO in your .env file. This value must be
set between 0 and 1 (for example, 0.9 is very tolerant).


## Misc documentation

- https://discord.com/developers/
- https://discord.js.org/
- https://discordjs.guide/
- https://www.startinop.com/gaming/discord-bot/
- https://mtxserv.com/fr/article/12098/creation_d_un_bot_discord_application_permissions
- https://medium.com/@mason.spr/hosting-a-discord-js-bot-for-free-using-heroku-564c3da2d23f


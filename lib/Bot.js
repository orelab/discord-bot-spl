const { Client, Intents } = require('discord.js');
const Moderation = require('./FilterModeration');
const Notice = require('./FilterNotice');
const PingPong = require('./FilterPingPong');
const Polite = require('./FilterPolite');
const Random = require('./FilterRandom');
const Missing = require('./FilterMissing');
const Question = require('./FilterQuestion');
const Coiffeur = require('./FilterCoiffeur');
const Google = require('./FilterGoogle');
const What = require('./FilterWhat');


module.exports = class Bot {

    filters = [
        new Notice(this),
        new PingPong(this),
        new Polite(this),
        new Random(this),
        new Missing(this),
        new Question(this),
        new Google(this),
        new What(this),
    ];

    // Discord token

    token = '';

    // Discord informations about bot

    bot = {};

    // Discord.js client

    client = {};

    // Discord informations about current channel (ex: #general)
    // Note that the channel contains its member list

    channel = {};


    constructor(token) {
        this.token = token;

        this.client = new Client({ partials:['MESSAGE', 'CHANNEL'], intents: [
            Intents.FLAGS.GUILDS,
            Intents.FLAGS.GUILD_MEMBERS,
            Intents.FLAGS.GUILD_PRESENCES,
            Intents.FLAGS.GUILD_MESSAGES,
            Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
            Intents.FLAGS.GUILD_MESSAGE_TYPING,
            Intents.FLAGS.DIRECT_MESSAGES,
            Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
            Intents.FLAGS.DIRECT_MESSAGE_TYPING,
        ] });

        this.client.once('ready', this.isReady.bind(this));
        this.client.on('messageCreate', this.routing.bind(this));
        this.client.on('error', this.error.bind(this));
        this.client.login(token);
    }


    isReady() {
        this.bot = this.client.user;
        console.log('Smith is here !');
    }

    routing(msg) {

        // filter Bot's own messages
        if (msg.author.id === this.bot.id) return;

        // bad filter
        const badFilter = new Coiffeur(this);
        badFilter.filter(msg);

        // Moderation (this filter must be tested even if
        // the receiver of this message is not the Bot)
        (new Moderation(this)).filter(msg).then(ok => {

            if(!ok) return;

            // Bot answers only if it is mentionned in a channel, or in DM
            if (msg.channel.type !== 'DM' && !this.bot_is_mentionned(msg)) return;

            this.clean_message(msg);
            console.log(msg.content);
            this.channel = this.client.channels.cache.get(msg.channel.id);

            for (let i = 0; i < this.filters.length; i++) {
                if (this.filters[i].filter(msg)) {
                    break;
                }
            }
        });
    }

    /*
        The bot must be mentionned in the message to work
    */
    bot_is_mentionned(msg) {
        return msg.mentions.users
            .map(u => u.id)
            .includes(this.bot.id);
    }

    /*
        Remove the user mentions from the message
        and misc other cleanings
    */
    clean_message(msg) {
        msg.content = msg.content
            .replace(/<@[0-9]*>/g, ' ')
            .trim()
            .toLowerCase();
    }


    /*
        Get the list of members, from the current channel
        from where the message is written
    */
    members() {
        if (!('members' in this.channel)) return [];

        return this.channel.members
            // Alina
            .filter(u => u.user.id != '691936803330195517')
            .filter(u => !u.permissions.has('ADMINISTRATOR'));
    }

    error(err) {
        console.log('An error occured...');
        console.error(err);
    }

    /*
        Say something (msg) in a specified channel (channel_id)
    */
    say(msg) {
        this.client.channels.cache.get(process.env.HELLO_CHANNEL).send(msg);
    }
};

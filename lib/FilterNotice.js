const Filter = require('./Filter');
// const { EmbedBuilder } = require('discord.js');


module.exports = class Notice extends Filter {

    startsWith = ['?', 'help', 'aide'];

    action(msg) {

        const notice =
            '                                                         \r\n' +
            '----------------------------------------------------     \r\n' +
            '                                                         \r\n' +
            '              COMMANDES DISPONIBLES                      \r\n' +
            '   (Le bot doit être mentionné pour fonctionner)         \r\n' +
            '                                                         \r\n' +
            '----------------------------------------------------     \r\n' +
            '                                                         \r\n' +
            '?         : affiche cet aide-mémoire                     \r\n' +
            'ping      : renvoie pong, juste pour tester              \r\n' +
            'bonjour   : répond poliment...                           \r\n' +
            'random x  : créée des groupes de x personnes parmis      \r\n' +
            '            les membres non-admin du groupe courant      \r\n' +
            'missing   : liste les personnes absentes ou              \r\n' +
            '            inactives du groupe courant                  \r\n' +
            '                                                         \r\n' +
            'Quand on lui demande gentillement un coup de main,       \r\n' +
            'il propose sympathiquement son aide... #RTFM             \r\n' +
            '                                                         \r\n' +
            'Ce bot est aussi capable de modérer le salon à l\'aide   \r\n' +
            'de l\'API Perspective (https://www.perspectiveapi.com)   \r\n' +
            '';


        // const embedNotice = new EmbedBuilder()
        //     .setColor(0x0099FF)
        //     .setTitle('Agent coincoin')
        //     .setDescription(notice);

        // msg.channel.send({ embeds: [embedNotice] });
        msg.reply(notice);

        return true;
    }
};


const Filter = require('./Filter');

module.exports = class Random extends Filter {

    startsWith = ['random'];

    action(msg) {
        let members = this.bot.members()
            .map(u => u.user.username)
            // shuffle
            .sort(() => 0.5 - Math.random());

        const params = msg.content.split(' ');

        if (params.length == 2) {

            const num = Number(params[1]);
            let str = '';

            for (let i = 0, j = 1; i < members.length; i += num) {
                str += `Groupe ${j++} : `
                    + members.slice(i, (i + num)).join(' / ')
                    + '\n';
            }
            members = str;
            msg.reply(members || 'Il n\'y a pas personne ici !');
        }
        else {
            let k = 0;
            msg.reply(members.map(u => ++k + ' - ' + u).join('\n') || 'nothing !');
        }

        return true;
    }

};
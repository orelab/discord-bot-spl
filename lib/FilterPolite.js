const Filter = require('./Filter');

module.exports = class Polite extends Filter {

    startsWith = ['coucou', 'hello', 'salut', 'bonjour'];

    action(msg) {
        let response = '';
        const who = msg.author.username;

        switch (Math.round(Math.random() * 10)) {
            case 0: response = `Comment ça va aujourd'hui, ${who} ?`; break;
            case 1: response = `Bonjour ${who} !`; break;
            case 2: response = `Hey, quoi de neuf, ${who} ?`; break;
            case 3: response = `Tu devrais pas bosser, ${who} ?`; break;
            case 4: response = `Ouai, bonjour, ${who}...`; break;
            case 5: response = 'GARDE À VOUS ! ! !'; break;
            case 6: response = `Qu'est-ce que tu veux, ${who} ?`; break;
            default: response = 'Bonjour !'; break;
        }
        msg.reply(response);

        return true;
    }

};